package Contest;

//class CAT8 yang diextend ke class abstract tiketKonser
//karena memiliki behavior (method) sama dengan parent class tersebut
class CAT8 extends TiketKonser {
    //Do your magic here...
    //Membuat method CAT8 yang berisi parameter nama dan harga
    public CAT8(String nama,double harga) {
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}