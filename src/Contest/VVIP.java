package Contest;

//class VVIP yang diextend ke class abstract TiketKonser
//karena memiliki bhavior sama (method) sama dengan parent class tersebut
class VVIP extends TiketKonser {
    // Do your magic here...

    //Membuat method VVIP yang berisi parameter nama dan harga
        public VVIP(String nama,double harga) {
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}