package Contest;

//class VIP yang diextend ke class abstract TiketKonser
//karena memiliki bhavior sama (method) sama dengan parent class tersebut
class VIP extends TiketKonser {
    // Do your magic here...

    //Membuat method VIP yang berisi parameter nama dan harga
        public VIP(String nama,double harga) {
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}