package Contest;


//class CAT1 yang diextend ke class abstract tiketKonser
//karena memiliki bhavior sama (method) sama dengan parent class tersebut
class CAT1 extends TiketKonser {
    //Do your magic here...
    //Membuat method CAT1 yang berisi parameter nama dan harga
    public CAT1(String nama,double harga) {
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}