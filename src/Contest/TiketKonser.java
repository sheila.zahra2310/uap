package Contest;

//membuat abstact class TiketKonser sebagai parent class, namun juga merangkap sebagai child class
//karena menjadi child dari interface class HargaTiket
abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
     String namaTiket;
     double hargaTiket;

     //mengembalikan nilai harga tiket
    @Override
    public double getHarga() {
        return this.hargaTiket;
    }
}