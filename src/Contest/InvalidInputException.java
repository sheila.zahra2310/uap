package Contest;

//membuat class untuk exception untuk mengubah eorr menjadi pesan eror
class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}