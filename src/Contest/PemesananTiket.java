package Contest;

import java.util.Scanner;

class PemesananTiket {
    // Do your magic here...

    //deklarasi
    static String namaPemesan;
    static TiketKonser dipesan;
    
    //
    public static void run() throws InvalidInputException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan nama pemesan : ");
        namaPemesan = sc.nextLine();

        //menggunakan if sebagai pengkondisian  apabila namaPemesan => 10 maka akan dilempar pada exception
        //yaitu InvalidInputException untuk memunculkan pesan eror pada user
        if (namaPemesan.length() >= 10) {
            throw new InvalidInputException("Nama pemesan harus kurang dari 10 karakter");
        }

        //untuk mencetak menu pilihan jenis-jenis tiket
        System.out.print("Pilih jenis tiket :");
        System.out.println("\n1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. UNLIMITED EXPERIENCE");
        System.out.println("masukkan pilihan :");
        int pilihan = sc.nextInt();

        //menggunakan if sebagai pengkondisian apabila pilihan < 1 atau pilihan > 5 maka akan dilempar pada exception
        //yaitu InvalidInputException untuk memunculkan pesan eror pada user
        if (pilihan < 1 || pilihan > 5) {
            throw new InvalidInputException("pilihan tiket harus antara 1 hingga 5");
        }

        //pengkondisian switch case untuk memilih salah 1 syarat,
        //apabila sudah masuk pada 1 case akan langsung keluar dari pengondisian
        switch (pilihan) {
            case 1:
                dipesan = new CAT8("CAT8", 800000);
                break;
            case 2:
                dipesan = new CAT1("CAT1", 3500000);
                break;
            case 3:
                dipesan = new FESTIVAL("FESTIVAL", 5000000);
                break;
            case 4:
                dipesan = new VIP("VIP", 65000000);
                break;
            case 5:
                dipesan = new VVIP("UNLIMITED EXPERIENCE", 8000000);
                break;
            default:
                break;
        }
    }
}