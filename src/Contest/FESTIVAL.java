package Contest;

////class FESTIVAL yang diextend ke class abstract tiketKonser
//karena memiliki behavior (method) dengan parent class tersebut
class FESTIVAL extends TiketKonser {
    //Do your magic here...
    //Membuat method FESTIVAL yang berisi parameter nama dan harga
    public FESTIVAL(String nama,double harga) {
        this.namaTiket = nama;
        this.hargaTiket = harga;
    }
}